# Zopa - Rate calculation system
Calculates the rate and payment amount of a requested amount for loan.

### Installation
Requires [Node.js](https://node.js.org) v6+ to run.

#### Run app
- Clone or download the repository 
- navigate to zopa folder 
```sh
cd zopa
node src/index.js $FILENAME $LOAN
```
or
```sh
npm run start $FILENAME $LOAN 
```