var fs = require('fs');

var loanCalculator = require('./lib/loanCalculator.js');
var parseData = require('./lib/parser.js');


LoanApproval = (filename, amount) => {

	const MAX_LOAN = 15000;
	const MIN_LOAN = 1000;

	/**
	 * Returns the content of the specific filename given
	 * If the file does not exist returns null
	 * @param {string} filename - The name of the file
	 */
	readFile = (filename) => {

		try {
			return fs.readFileSync(filename).toString();
		} catch (ex) {
			console.log('Please provide a valid file.');
			return null;
		}
	}

	/**
	 * Validates the requested amount given
	 * based on the following validation rules
	 * @param {number} amount - The requested loan amount
	 */
	validateAmount = (amount) => {

		if (amount > MAX_LOAN){
			console.log(`You can request loan less than 15000 but you have requested £${amount}`);
		} else if (amount < MIN_LOAN){
			console.log(`You can request loan more than 1000 but you have requested £${amount}`);
		} else if (amount%100 !== 0){
			console.log('You should provide an amount increment by 100 between £1000 and £15000');
		} else {
			return true;
		}
		return false;
	}

	((filename, requestedAmount) => {

		var content = readFile(filename);
		if(!content) {
			return;
		}

		if(!validateAmount(requestedAmount)){
			return;
		}

		var lenders = parseData.parse(content);
		var availableResources = lenders
								.map(function(lender) {return lender.available})
								.reduce(function (a, b) {return a+b});

		if(requestedAmount > availableResources) {
			console.log('Currently our available resources cannot serve your request.');
			return;
		}

		var data = loanCalculator.calculateLoan(lenders, requestedAmount);
		console.log(`Requested Amount: £${data.requestedAmount}`);
		console.log(`Rate: ${data.rate}%`);
		console.log(`Monthly Repayment: £${data.monthPayment}`);
		console.log(`Total Repayment: £${data.totalPayment}`);

	})(filename, amount);
}

((args) => {

	args = args.slice(2);
	if (args.length < 2) {
		console.log('Please provide a file with the pool of borrowers and the requested loan amount.');
		return;
	}

	var filename = args.shift();
	var amount = args.shift();

	LoanApproval(filename, amount);

})(process.argv);
