/*
 * Calculates for the requested loan amount the rate, the monthly and total repayment amounts
 * based on the available lenders
 * param {array} lenders - the lenders with their name, rate and available amount for lend
 * param {number} requestedAmount - the requested loan amount
 */
const PERIODIC_PAYMENTS = 36;
const MONTHS = 12;
calculateApproval = (lenders, requestedAmount) => {

	//sort lenders based on the lower rate
	lenders.sort(function sortLenders(lenderA, lenderB) {
		return lenderA.rate - lenderB.rate;
	});

	// Loan Payment = Amount/Discount Factor
	// Number of Periodic Payments (n) = Payments per year times number of years
	// Periodic Interest Rate (i) = Annual rate divided by number of payment periods
	// Discount Factor (D) = {[(1 + i)^n] - 1} / [i(1 + i)^n]
	var x = rate = funded = 0;
	var paymentPerMonth = 0;
	var remainingForFund = requestedAmount;

	while (funded < requestedAmount) {

		var lender = lenders[x];
		var interestRate = lender.rate/MONTHS;
		var discountFactor = (Math.pow(1+interestRate, PERIODIC_PAYMENTS)-1)/(interestRate*Math.pow(1+interestRate, PERIODIC_PAYMENTS));

		var currentFund;
		if (remainingForFund - lender.available >= 0) {
			currentFund = lender.available;
		} else {
			currentFund = remainingForFund;
		}

		funded += currentFund;
		remainingForFund -= currentFund;
		rate += lender.rate;

		paymentPerMonth += currentFund/discountFactor;
		x++;
	}

	var totalLoan = paymentPerMonth*PERIODIC_PAYMENTS;
	var averageRate = rate*100/x;

	return {
		requestedAmount: requestedAmount,
		rate: averageRate.toFixed(1),
		monthPayment: paymentPerMonth.toFixed(2),
		totalPayment: totalLoan.toFixed(2)
	}
}

module.exports = {
	calculateLoan: calculateApproval
}