/**
 * Parses data
 * @param {number} data
 */
parseData = (data) => {

	data = data.toString().split('\n');

	// remove header
	data.shift();

	var lenders = [];
	data.forEach((line) => {
		var rawLender = line.toString().split('\t');

		// enrich data
		var lender = {
			name: rawLender[0],
			rate: parseFloat(rawLender[1]),
			available: parseInt(rawLender[2].split('\r')[0])
		}
		lenders.push(lender);
	});
	return lenders;
}

module.exports = {
	parse: parseData
}